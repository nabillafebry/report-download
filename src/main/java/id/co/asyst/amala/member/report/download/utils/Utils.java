package id.co.asyst.amala.member.report.download.utils;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import id.co.asyst.amala.member.report.download.model.airaccrual.AirAccrual;
import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {
    private static Logger logger = LogManager.getLogger(Utils.class);

    public void getBody(Exchange exchange){
        Map<String, Object> bodyQueue = (Map<String, Object>) exchange.getProperty("bodyQueue");

        String type = (String) bodyQueue.get("type");
        String query = (String) bodyQueue.get("query");
        String userid = (String) bodyQueue.get("userid");

        exchange.setProperty("type", type);
        exchange.setProperty("query", query);
        exchange.setProperty("userid", userid);
    }
}
