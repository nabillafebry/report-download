package id.co.asyst.amala.member.report.download.utils;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import com.opencsv.CSVWriter;
import id.co.asyst.amala.member.report.download.model.airaccrual.AirAccrual;
import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class UtilsAirAccrual {
    private static Logger logger = LogManager.getLogger(UtilsAirAccrual.class);

    @Value("${file.location}")
    private String path;

    public void selectBigQuery(Exchange exchange){
        String queryString = (String) exchange.getProperty("query");
        AirAccrual airAccrual = new AirAccrual();
        List<AirAccrual> listAiraccrual = new ArrayList<>();
        HashMap<String, String> accruals = new HashMap<>();
        String query = queryString;
        String gcp = path + "/amala-prd-4a58450b1f18.json";
//        String gcp = "D://AMALA/report-app/gcp_keys/prod/amala-prd-4a58450b1f18.json";
        try {
            BigQuery bigquery = BigQueryOptions.newBuilder().setProjectId("amala-prd")
                    .setCredentials(
                            ServiceAccountCredentials.fromStream(new FileInputStream(gcp))
                    ).build().getService();

            QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query).build();

            TableResult result = null;
            result = bigquery.query(queryConfig);
            for (FieldValueList row : bigquery.query(queryConfig).iterateAll()) {
                accruals = new HashMap<>();
                airAccrual = new AirAccrual();
                for (int i = 0; i < result.getSchema().getFields().size(); i++) {
                    if (row.get(i).getValue() != null) {
                        accruals.put(result.getSchema().getFields().get(i).getName(), row.get(i).getValue().toString());
                    } else {
                        accruals.put(result.getSchema().getFields().get(i).getName(), "-");
                    }
                }
                airAccrual = setValAccrual(airAccrual, accruals);

                listAiraccrual.add(airAccrual);

            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        exchange.setProperty("listResult", listAiraccrual);
    }

    public void generateFile(Exchange exchange){
        List<Map<String, String>> airAccrualList = (List<Map<String, String>>) exchange.getProperty("listResult");
        String userid = (String) exchange.getProperty("userid");
        String filelocation = path + "/airaccrual";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime now = LocalDateTime.now();
        String datenow = dtf.format(now);
        String filename = "/airaccrual_" + datenow + "_" + userid + ".csv";
        String[] header = {"transaction_id", "insert_date", "membership_number", "tier", "name", "surname", "email_address",
                "partner", "activity_classification", "dot", "org", "dest", "sector", "marketing_flight_number",
                "operating_flight_number", "booking_class", "tier_points", "award_points", "billing_base_points",
                "billing_bonus_points", "pax_name", "marketing_airline","operating_airline", "booking_code", "ticket_number",
                "flown_class", "frequency", "member_status", "inserted_by", "bo_code", "transaction_type", "ticketing_office"};

        SimpleDateFormat formatcontext = new SimpleDateFormat("dd-MM-yyyy");


        File file = new File(filelocation + filename);
        // create FileWriter object with file as parameter
        FileWriter outputfile = null;
        try {
            outputfile = new FileWriter(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // create CSVWriter with '|' as separator
        CSVWriter writer = new CSVWriter(outputfile, '|',
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);

        writer.writeNext(header);
        for (Map<String, String> airAccrual : airAccrualList) {
            String[] stringAccrual = new String[32];
            stringAccrual[0] = airAccrual.get("transaction_id");
            stringAccrual[1] = airAccrual.get("insert_date");
            stringAccrual[2] = airAccrual.get("membership_number");
            stringAccrual[3] = airAccrual.get("tier");
            stringAccrual[4] = airAccrual.get("name");
            stringAccrual[5] = airAccrual.get("surname");
            stringAccrual[6] = airAccrual.get("email_address");
            stringAccrual[7] = airAccrual.get("partner");
            stringAccrual[8] = airAccrual.get("activity_classification");
            stringAccrual[9] = airAccrual.get("dot");
            stringAccrual[10] = airAccrual.get("org");
            stringAccrual[11] = airAccrual.get("dest");
            stringAccrual[12] = airAccrual.get("sector");
            stringAccrual[13] = airAccrual.get("marketing_flight_number");
            stringAccrual[14] = airAccrual.get("operating_flight_number");
            stringAccrual[15] = airAccrual.get("booking_class");
            stringAccrual[16] = airAccrual.get("tier_points");
            stringAccrual[17] = airAccrual.get("award_points");
            stringAccrual[18] = airAccrual.get("billing_base_points");
            stringAccrual[19] = airAccrual.get("billing_bonus_points");
            stringAccrual[20] = airAccrual.get("pax_name");
            stringAccrual[21] = airAccrual.get("marketing_airline");
            stringAccrual[22] = airAccrual.get("operating_airline");
            stringAccrual[23] = airAccrual.get("booking_code");
            stringAccrual[24] = airAccrual.get("ticket_number");
            stringAccrual[25] = airAccrual.get("flown_class");
            stringAccrual[26] = airAccrual.get("frequency");
            stringAccrual[27] = airAccrual.get("member_status");
            stringAccrual[28] = airAccrual.get("inserted_by");
            stringAccrual[29] = airAccrual.get("bo_code");
            stringAccrual[30] = airAccrual.get("transaction_type");
            stringAccrual[31] = airAccrual.get("ticketing_office");


            writer.writeNext(stringAccrual);
        }
    }

    public AirAccrual setValAccrual(AirAccrual airAccrual, HashMap<String, String> accrual) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date insertdate = format.parse(accrual.get("insert_date"));
        Date dot = format.parse(accrual.get("dot"));
        int tierpoints = Integer.parseInt(accrual.get("tier_points"));
        int awardpoints = Integer.parseInt(accrual.get("award_points"));
        if (!StringUtils.isEmpty(accrual.get("billing_base_points")) && !accrual.get("billing_base_points").equals("-")) {
            BigInteger billingbase = new BigInteger(accrual.get("billing_base_points"));
            airAccrual.setBillingbasepoints(billingbase);
        }
        if (!StringUtils.isEmpty(accrual.get("billing_bonus_points")) && !accrual.get("billing_bonus_points").equals("-")) {
            BigInteger billingbonus = new BigInteger(accrual.get("billing_bonus_points"));
            airAccrual.setBillingbonuspoints(billingbonus);
        }
        int frequency = Integer.parseInt(accrual.get("frequency"));

        airAccrual.setTransactionid(accrual.get("transaction_id"));
        airAccrual.setInsertdate(insertdate);
        airAccrual.setMembershipnumber(accrual.get("membership_number"));
        airAccrual.setTier(accrual.get("tier"));
        airAccrual.setName(accrual.get("name"));
        airAccrual.setSurname(accrual.get("surname"));
        airAccrual.setEmailaddress(accrual.get("email_address"));
        airAccrual.setPartner(accrual.get("partner"));
        airAccrual.setActivityclassification(accrual.get("activity_classification"));
        airAccrual.setDot(dot);
        airAccrual.setOrg(accrual.get("org"));
        airAccrual.setDest(accrual.get("dest"));
        airAccrual.setSector(accrual.get("sector"));
        airAccrual.setMarketingflightnumber(accrual.get("marketing_flight_number"));
        airAccrual.setBookingclass(accrual.get("booking_class"));
        airAccrual.setTierpoints(tierpoints);
        airAccrual.setAwardpoints(awardpoints);
        airAccrual.setPaxname(accrual.get("pax_name"));
        airAccrual.setMarketingairline(accrual.get("marketing_airline"));
        airAccrual.setOperatingairline(accrual.get("operating_airline"));
        airAccrual.setBookingcode(accrual.get("booking_code"));
        airAccrual.setTicketnumber(accrual.get("ticket_number"));
        airAccrual.setFlownclass(accrual.get("flown_class"));
        airAccrual.setFrequency(frequency);
        airAccrual.setMemberstatus(accrual.get("member_status"));
        airAccrual.setInsertedby(accrual.get("inserted_by"));
        airAccrual.setBocode(accrual.get("bo_code"));
        airAccrual.setOperatingflightnumber(accrual.get("operating_flight_number"));
        airAccrual.setTransactiontype(accrual.get("transaction_type"));
        airAccrual.setTicketingoffice(accrual.get("ticketing_office"));

        return airAccrual;
    }


}
