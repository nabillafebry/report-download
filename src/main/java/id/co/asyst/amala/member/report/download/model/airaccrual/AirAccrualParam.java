package id.co.asyst.amala.member.report.download.model.airaccrual;


import java.util.Map;

public class AirAccrualParam {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -1674347067116782629L;

    AirAccrualRequest criteria;

    Map<String, String> sort;

    public AirAccrualRequest getCriteria() {
        return criteria;
    }

    public void setCriteria(AirAccrualRequest criteria) {
        this.criteria = criteria;
    }

    public Map<String, String> getSort() {
        return sort;
    }

    public void setSort(Map<String, String> sort) {
        this.sort = sort;
    }
}
