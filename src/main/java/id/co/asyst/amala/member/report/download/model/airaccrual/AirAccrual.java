package id.co.asyst.amala.member.report.download.model.airaccrual;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.asyst.amala.core.payload.CustomDateDeserializer;
import id.co.asyst.amala.core.payload.CustomDateSerializer;
import id.co.asyst.commons.core.model.BaseModel;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
//@Table(name = airaccrual.AIR_ACCRUAL)
@JsonInclude
@JsonIgnoreProperties(value = {"updatedBy", "updatedDate", "createdBy", "createdDate"})
public class AirAccrual extends BaseModel {

    /**
     * Column name for Transaction ID.
     */
    public static final String COLUMN_TRANSACTION_ID = "transaction_id";

//    /* Constants: */
//    /**
//     * View name.
//     */
//    public static final String AIR_ACCRUAL = "air_accrual_view";
    /**
     * Column Name for Insert Date.
     */
    public static final String COLUMN_INSERT_DATE = "insert_date";
    /**
     * Column Name for Membership Number.
     */
    public static final String COLUMN_MEMBERSHIP_NUMBER = "membership_number";
    /**
     * Column Name for Tier.
     */
    public static final String COLUMN_TIER = "tier";
    /**
     * Column Name for Name.
     */
    public static final String COLUMN_NAME = "name";
    /**
     * Column Name for Surname.
     */
    public static final String COLUMN_SURNAME = "surname";
    /**
     * Column Name for Email Address).
     */
    public static final String COLUMN_EMAIL_ADDRESS = "email_address";
    /**
     * Column Name for Partner.
     */
    public static final String COLUMN_PARTNER = "partner";
    /**
     * Column Name for Activity Classification.
     */
    public static final String COLUMN_ACTIVITY_CLASSIFICATION = "activity_classification";
    /**
     * Column Name for Dot.
     */
    public static final String COLUMN_DOT = "dot";
    /**
     * Column Name for Org.
     */
    public static final String COLUMN_ORG = "org";
    /**
     * Column Name for Dest.
     */
    public static final String COLUMN_DEST = "dest";
    /**
     * Column Name for Sector.
     */
    public static final String COLUMN_SECTOR = "sector";
    /**
     * Column Name for Marketing Flight Number.
     */
    public static final String COLUMN_MARKETING_FLIGHT_NUMBER = "marketing_flight_number";
    public static final String COLUMN_OPERATING_FLIGHT_NUMBER = "operating_flight_number";
    /**
     * Column Name for Booking Class.
     */
    public static final String COLUMN_BOOKING_CLASS = "booking_class";
    /**
     * Column Name for Tier Points.
     */
    public static final String COLUMN_TIER_POINTS = "tier_points";
    /**
     * Column Name for Award Points.
     */
    public static final String COLUMN_AWARD_POINTS = "award_points";
    /**
     * Column Name for Billing Base Points.
     */
    public static final String COLUMN_BILLING_BASE_POINTS = "billing_base_points";
    /**
     * Column Name for Billing Bonus Points.
     */
    public static final String COLUMN_BILLING_BONUS_POINTS = "billing_bonus_points";
    /**
     * Column Name for Billing Pax Name.
     */
    public static final String COLUMN_PAX_NAME = "pax_name";
    /**
     * Column Name for Marketing Airline.
     */
    public static final String COLUMN_MARKETING_AIRLINE = "marketing_airline";
    /**
     * Column Name for Operating Airline.
     */
    public static final String COLUMN_OPERATING_AIRLINE = "operating_airline";
    /**
     * Column Name for Booking Code.
     */
    public static final String COLUMN_BOOKING_CODE = "booking_code";
    /**
     * Column Name for Ticket Number.
     */
    public static final String COLUMN_TICKET_NUMBER = "ticket_number";
    /**
     * Column Name for Flown Class.
     */
    public static final String COLUMN_FLOWN_CLASS = "flown_class";
    /**
     * Column Name for Frequency.
     */
    public static final String COLUMN_FREQUENCY = "frequency";
    /**
     * Column Name for Member Status.
     */
    public static final String COLUMN_MEMBER_STATUS = "member_status";
    /**
     * Column Name for Created By.
     */
    public static final String COLUMN_INSERTED_BY = "inserted_by";
    /**
     * Column Name for Branch Office Code.
     */
    public static final String COLUMN_BRANCH_OFFICE_CODE = "bo_code";

    public static final String COLUMN_TRANSACTION_TYPE = "transaction_type";

    public static final String COLUMN_TICKETING_OFFICE = "ticketing_office";
    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -1674347067116782629L;


    /* Attributes: */
    /**
     * Transaction ID.
     */
    @Id
    @Column(name = COLUMN_TRANSACTION_ID)
    private String transactionid;

    /**
     * Created Date.
     */
    @Column(name = COLUMN_INSERT_DATE)
    @Temporal(TemporalType.DATE)
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date insertdate;
    /**
     * Membership Number.
     */
    @Column(name = COLUMN_MEMBERSHIP_NUMBER)
    private String membershipnumber;
    /**
     * Tier.
     */
    @Column(name = COLUMN_TIER)
    private String tier;

    /**
     * Name.
     */
    @Column(name = COLUMN_NAME)
    private String name;

    /**
     * Surname.
     */
    @Column(name = COLUMN_SURNAME)
    private String surname;

    /**
     * Email Address.
     */
    @Column(name = COLUMN_EMAIL_ADDRESS)
    private String emailaddress;

    /**
     * Partner.
     */
    @Column(name = COLUMN_PARTNER)
    private String partner;

    /**
     * Activity Classification.
     */
    @Column(name = COLUMN_ACTIVITY_CLASSIFICATION)
    private String activityclassification;

    /**
     * Dot.
     */
    @Column(name = COLUMN_DOT)
    @Temporal(TemporalType.DATE)
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date dot;

    /**
     * Org.
     */
    @Column(name = COLUMN_ORG)
    private String org;

    /**
     * Dest.
     */
    @Column(name = COLUMN_DEST)
    private String dest;

    /**
     * Sector.
     */
    @Column(name = COLUMN_SECTOR)
    private String sector;

    /**
     * Marketing Flight Numbers.
     */
    @Column(name = COLUMN_MARKETING_FLIGHT_NUMBER)
    private String marketingflightnumber;


    @Column(name = COLUMN_OPERATING_FLIGHT_NUMBER)
    private String operatingflightnumber;

    /**
     * Booking Class.
     */
    @Column(name = COLUMN_BOOKING_CLASS)
    private String bookingclass;

    /**
     * Tier Points.
     */
    @Column(name = COLUMN_TIER_POINTS)
    private Integer tierpoints;

    /**
     * Award Points.
     */
    @Column(name = COLUMN_AWARD_POINTS)
    private Integer awardpoints;
    /**
     * Billing Base Point.
     */
    @Column(name = COLUMN_BILLING_BASE_POINTS)
    private BigInteger billingbasepoints;
    /**
     * Billing Bonus Point.
     */
    @Column(name = COLUMN_BILLING_BONUS_POINTS)
    private BigInteger billingbonuspoints;

    /**
     * Pax Name.
     */
    @Column(name = COLUMN_PAX_NAME)
    private String paxname;

    /**
     * Marketing Airline.
     */
    @Column(name = COLUMN_MARKETING_AIRLINE)
    private String marketingairline;

    /**
     * Operating Airline.
     */
    @Column(name = COLUMN_OPERATING_AIRLINE)
    private String operatingairline;

    /**
     * Booking Code.
     */
    @Column(name = COLUMN_BOOKING_CODE)
    private String bookingcode;

    /**
     * Ticket Number.
     */
    @Column(name = COLUMN_TICKET_NUMBER)
    private String ticketnumber;

    /**
     * Flown Class.
     */
    @Column(name = COLUMN_FLOWN_CLASS)
    private String flownclass;

    /**
     * Frequency.
     */
    @Column(name = COLUMN_FREQUENCY)
    private Integer frequency;

    /**
     * Member Status.
     */
    @Column(name = COLUMN_MEMBER_STATUS)
    private String memberstatus;
    /**
     * Inserted By.
     */
    @Column(name = COLUMN_INSERTED_BY)
    private String insertedby;
    /**
     * Branch Office Code.
     */
    @Column(name = COLUMN_BRANCH_OFFICE_CODE)
    private String bocode;


    @Column(name = COLUMN_TRANSACTION_TYPE)
    private String transactiontype;

    @Column(name = COLUMN_TICKETING_OFFICE)
    private String ticketingoffice;

    /**
     * Operating Marketing Flight Numbers.
     */

    /**
     * Constructor
     */
    public AirAccrual() {
        //        this.transactionid = map.get("transactionid");
//        this.insertdate = map.get("insert_date");
//        this.membershipnumber = map.get("membership_number");
//        this.tier = map.get("tier");
//        this.name = map.get("name");
//        this.surname = map.get("surname");
//        this.emailaddress = map.get("email_address");
//        this.partner = map.get("partner");
//        this.activityclassification = map.get("activity_classification");
//        this.dot = map.get("dot");
//        this.org = map.get("org");
//        this.dest = map.get("dest");
//        this.sector = map.get("sector");
//        this.marketingflightnumber = map.get("marketing_flight_number");
//        this.bookingclass = map.get("booking_class");
//        this.tierpoints = map.get("tier_points");
//        this.awardpoints = map.get("award_points");
//        this.billingbasepoints = map.get("billing_base_points");
//        this.billingbonuspoints = map.get("billing_bonus_points");
//        this.paxname = map.get("pax_name");
//        this.marketingairline = map.get("marketing_airline");
//        this.operatingairline = map.get("operating_airline");
//        this.bookingcode = map.get("booking_code");
//        this.ticketnumber = map.get("ticket_number");
//        this.flownclass = map.get("flown_class");
//        this.frequency = map.get("frequency");
//        this.memberstatus = map.get("member_status");
//        this.insertedby = map.get("inserted_by");
//        this.bocode = map.get("bo_code");
//        this.operatingmarketingflightnumber = map.get("operating_marketing_flight_number");

    }

    /**
     * Getter And Setter
     */
    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public Date getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(Date insertdate) {
        this.insertdate = insertdate;
    }

    public String getMembershipnumber() {
        return membershipnumber;
    }

    public void setMembershipnumber(String membershipnumber) {
        this.membershipnumber = membershipnumber;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getActivityclassification() {
        return activityclassification;
    }

    public void setActivityclassification(String activityclassification) {
        this.activityclassification = activityclassification;
    }

    public Date getDot() {
        return dot;
    }

    public void setDot(Date dot) {
        this.dot = dot;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getMarketingflightnumber() {
        return marketingflightnumber;
    }

    public void setMarketingflightnumber(String marketingflightnumber) {
        this.marketingflightnumber = marketingflightnumber;
    }

    public String getOperatingflightnumber() {
        return operatingflightnumber;
    }

    public void setOperatingflightnumber(String operatingflightnumber) {
        this.operatingflightnumber = operatingflightnumber;
    }

    public String getBookingclass() {
        return bookingclass;
    }

    public void setBookingclass(String bookingclass) {
        this.bookingclass = bookingclass;
    }

    public Integer getTierpoints() {
        return tierpoints;
    }

    public void setTierpoints(Integer tierpoints) {
        this.tierpoints = tierpoints;
    }

    public Integer getAwardpoints() {
        return awardpoints;
    }

    public void setAwardpoints(Integer awardpoints) {
        this.awardpoints = awardpoints;
    }

    public BigInteger getBillingbasepoints() {
        return billingbasepoints;
    }

    public void setBillingbasepoints(BigInteger billingbasepoints) {
        this.billingbasepoints = billingbasepoints;
    }

    public BigInteger getBillingbonuspoints() {
        return billingbonuspoints;
    }

    public void setBillingbonuspoints(BigInteger billingbonuspoints) {
        this.billingbonuspoints = billingbonuspoints;
    }

    public String getPaxname() {
        return paxname;
    }

    public void setPaxname(String paxname) {
        this.paxname = paxname;
    }

    public String getMarketingairline() {
        return marketingairline;
    }

    public void setMarketingairline(String marketingairline) {
        this.marketingairline = marketingairline;
    }

    public String getOperatingairline() {
        return operatingairline;
    }

    public void setOperatingairline(String operatingairline) {
        this.operatingairline = operatingairline;
    }

    public String getBookingcode() {
        return bookingcode;
    }

    public void setBookingcode(String bookingcode) {
        this.bookingcode = bookingcode;
    }

    public String getTicketnumber() {
        return ticketnumber;
    }

    public void setTicketnumber(String ticketnumber) {
        this.ticketnumber = ticketnumber;
    }

    public String getFlownclass() {
        return flownclass;
    }

    public void setFlownclass(String flownclass) {
        this.flownclass = flownclass;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public String getMemberstatus() {
        return memberstatus;
    }

    public void setMemberstatus(String memberstatus) {
        this.memberstatus = memberstatus;
    }

    public String getInsertedby() {
        return insertedby;
    }

    public void setInsertedby(String insertedby) {
        this.insertedby = insertedby;
    }

    public String getBocode() {
        return bocode;
    }

    public void setBocode(String bocode) {
        this.bocode = bocode;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getTicketingoffice() {
        return ticketingoffice;
    }

    public void setTicketingoffice(String ticketingoffice) {
        this.ticketingoffice = ticketingoffice;
    }

    /**
     * ToString Override
     */

    @Override
    public String toString() {
        return "airaccrual{" +
                "transactionid='" + transactionid + '\'' +
                ", insertdate=" + insertdate +
                ", membershipnumber='" + membershipnumber + '\'' +
                ", tier='" + tier + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", emailaddress='" + emailaddress + '\'' +
                ", partner='" + partner + '\'' +
                ", activityclassification='" + activityclassification + '\'' +
                ", dot=" + dot +
                ", org='" + org + '\'' +
                ", dest='" + dest + '\'' +
                ", sector='" + sector + '\'' +
                ", marketingflightnumber='" + marketingflightnumber + '\'' +
                ", bookingclass='" + bookingclass + '\'' +
                ", tierpoints=" + tierpoints +
                ", awardpoints=" + awardpoints +
                ", billingbasepoints=" + billingbasepoints +
                ", billingbonuspoints=" + billingbonuspoints +
                ", paxname='" + paxname + '\'' +
                ", marketingairline='" + marketingairline + '\'' +
                ", operatingairline='" + operatingairline + '\'' +
                ", bookingcode='" + bookingcode + '\'' +
                ", ticketnumber='" + ticketnumber + '\'' +
                ", flownclass='" + flownclass + '\'' +
                ", frequency=" + frequency +
                ", memberstatus='" + memberstatus + '\'' +
                ", insertedby='" + insertedby + '\'' +
                ", bocode='" + bocode + '\'' +
                '}';
    }
}
