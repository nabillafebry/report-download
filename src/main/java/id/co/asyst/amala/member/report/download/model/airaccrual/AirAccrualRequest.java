package id.co.asyst.amala.member.report.download.model.airaccrual;


import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

public class AirAccrualRequest {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -1674347067116782629L;

    private Date insertdatefrom;
    private Date insertdateto;
    private Date activitydatefrom;
    private Date activitydateto;
    private List<String> partner;
    private List<String> airlines;
    private List<String> origin;
    private List<String> destination;
    private String memberstatus;
    private String compartmentclass;
    private String subclass;
    private String tier;
    private String cardnumber;
    private String branchoffice;
    private String ticketingoffice;

    @Transient
    private String decodetoken;

    @Transient
    private String msg;

    @Transient
    private String filename;

    public String getTicketingoffice() {
        return ticketingoffice;
    }

    public void setTicketingoffice(String ticketingoffice) {
        this.ticketingoffice = ticketingoffice;
    }

    public Date getInsertdatefrom() {
        return insertdatefrom;
    }

    public void setInsertdatefrom(Date insertdatefrom) {
        this.insertdatefrom = insertdatefrom;
    }

    public Date getInsertdateto() {
        return insertdateto;
    }

    public void setInsertdateto(Date insertdateto) {
        this.insertdateto = insertdateto;
    }

    public Date getActivitydatefrom() {
        return activitydatefrom;
    }

    public void setActivitydatefrom(Date activitydatefrom) {
        this.activitydatefrom = activitydatefrom;
    }

    public Date getActivitydateto() {
        return activitydateto;
    }

    public void setActivitydateto(Date activitydateto) {
        this.activitydateto = activitydateto;
    }

    public List<String> getPartner() {
        return partner;
    }

    public void setPartner(List<String> partner) {
        this.partner = partner;
    }

    public List<String> getAirlines() {
        return airlines;
    }

    public void setAirlines(List<String> airlines) {
        this.airlines = airlines;
    }

    public List<String> getOrigin() {
        return origin;
    }

    public void setOrigin(List<String> origin) {
        this.origin = origin;
    }

    public List<String> getDestination() {
        return destination;
    }

    public void setDestination(List<String> destination) {
        this.destination = destination;
    }

    public String getMemberstatus() {
        return memberstatus;
    }

    public void setMemberstatus(String memberstatus) {
        this.memberstatus = memberstatus;
    }

    public String getCompartmentclass() {
        return compartmentclass;
    }

    public void setCompartmentclass(String compartmentclass) {
        this.compartmentclass = compartmentclass;
    }

    public String getSubclass() {
        return subclass;
    }

    public void setSubclass(String subclass) {
        this.subclass = subclass;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getBranchoffice() {
        return branchoffice;
    }

    public void setBranchoffice(String branchoffice) {
        this.branchoffice = branchoffice;
    }

    public String getDecodetoken() {
        return decodetoken;
    }

    public void setDecodetoken(String decodetoken) {
        this.decodetoken = decodetoken;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
